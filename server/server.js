const http = require('http');
const path = require('path');
const express = require ('express');
const socketIO = require('socket.io');
const app = express()
const server = http.createServer(app)
const io = socketIO(server)
const { makeId } = require('./utils')

const { gameLoop, getUpdatedVelocity, initGame } = require('./game')
const { FRAME_RATE } = require('./constants');


const state = {}
const clientRooms = {}

io.on('connection', (client) => {
    

    client.on('keydown', (keyCode) => {
        const roomName = clientRooms[client.id]

        if(!roomName){
            return
        }

        try {
            keyCode = parseInt(keyCode)
        } catch (error) {
            console.error(error)
            return;
        }

        const vel = getUpdatedVelocity(keyCode);
        if(vel) {
            state[roomName].players[client.number-1].vel = vel
        }
    })

    client.on('newGame', () => {
        console.log('new Game client ID ', client.id)
        let roomName = makeId(5)
        clientRooms[client.id] = roomName
        console.log("emit rooName: ", roomName)
        client.emit('gameCode', roomName)

        state[roomName] = initGame()

        client.join(roomName)
        client.number = 1
        client.emit('init', 1)

    })

    client.on('joinGame', (gameCode) => {
        console.log(' adapter', io.sockets.adapter.rooms.get(gameCode))
        console.log(' adapter', io.sockets.adapter.rooms)
        const room = io.sockets.adapter.rooms.get(gameCode);
        console.log('roommmm : ', room)
        let allUsers;

        if(room){
            allUsers = Array.from(room)
        }

        let numClients = 0
        if (allUsers) {
            console.log('rooommmmsss legth', allUsers.length)
            numClients = allUsers.length
        }

        if(numClients === 0 ) {
            client.emit('unknownGame')
            return;
        } else if ( numClients > 1 ) {
            client.emit('tooManyPlayers')
            return;
        }

        clientRooms[client.id] = gameCode;

        client.join(gameCode)
        client.number = 2 
        client.emit('init', 2)

        startGameInterval(gameCode)

    })
    
})

function startGameInterval (roomName) {
    const IntervalId = setInterval (() => {
        const winner = gameLoop(state[roomName]);
        if(!winner){
            emitGameState(roomName, state[roomName])
        } else {
            emitGameOver (roomName, state[roomName])
            state[roomName] = null;
            clearInterval(IntervalId)
        }
    }, 1000 /FRAME_RATE) 
}


function emitGameState (roomName, state) {
    console.log(state.players[0].snake)
    io.sockets.in(roomName).emit('gameState', JSON.stringify(state))
}

function emitGameOver(roomName, winner) {
    io.sockets.in(roomName).emit('gameOver', JSON.stringify({winner}))
}

app.get('/', (req, res) => {

    res.sendFile(path.resolve(__dirname, '../', 'frontend', 'index.html'))
})

app.get('/index.js', (req, res) => {
    console.log('render index')
    res.sendFile(path.resolve(__dirname, '../', 'frontend', 'index.js'))
})

server.listen(3000, () => 
console.log(' Server listening on port 3000'));
